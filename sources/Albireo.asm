

;Register base address.
ACE_ADDR	equ 0xfeb0	; IO base address
ACE_ADDRh	equ 0xfe
ACE_ADDRl	equ 0xb0
ACE_CMDl        equ #81
ACE_DATl        equ #80
ACE_CMD         equ ACE_ADDRh * 256 + ACE_CMDl
ACE_DAT         equ ACE_ADDRh * 256 + ACE_DATl

ACE_SCR_OFF	equ 7     	; Scratch register
ACE_SCR		equ ACE_ADDR+ACE_SCR_OFF

;Anything will do.
ACE_MAGIC_NUMBER    equ #61


;Detects and initializes the Albireo.
;OUT:   Carry = 1 = OK. 0 if undetected or a problem occurred.
InitializeAlbireo:
        ld bc,ACE_SCR                   ;SIO_DETECT_INTERFACE
        ld a,ACE_MAGIC_NUMBER
        out (c),a
        in a,(c)
        xor ACE_MAGIC_NUMBER            ;Clears carry flag.
        jr nz,.error  ; Not found!               

        ;Basic: 100
        ld a,#15 : call AlbireoCmd
        ld a,#7 : call AlbireoDat

        ld b,180 : djnz $               ;Waits at least 10microsecs, else the communication will fail on cold reset.
        
        ;Basic: 110
        ld a,#15 : call AlbireoCmd
        ld a,#6 : call AlbireoDat
        ;Basic: 120
        ld a,#b : call AlbireoCmd
        ld a,#17 : call AlbireoDat
        ld a,#d8 : call AlbireoDat
        ;Basic: 130
        ld a,#45 : call AlbireoCmd
        ld a,#1 : call AlbireoDat
        
        ;Basic 140
        call AlbireoWaitForCommandEnd
        
        ;Basic: 150
        ld a,#13 : call AlbireoCmd
        ld a,#1 : call AlbireoDat
        ;Basic: 170
        ld a,#49 : call AlbireoCmd
        ld a,#1 : call AlbireoDat
        ;Basic 180
        call AlbireoWaitForCommandEnd

        ;Submit our first request to the mouse to get things started
        ;Basic 230
        ld a,#4e : call AlbireoCmd
       
        ;Basic 250
        ld a,0
        call AlbireoDat
        ld a,(Token + 1)
        xor #80
        ld (Token + 1),a
       
        ;Basic 300
        ld a,#19 : call AlbireoDat
        
        scf
        ret
.error:
        or a
        ret


        
;Sends a byte to the DAT port.
;IN:    A = the byte to send.
AlbireoDat:
        ld bc,ACE_DAT
        out (c),a
        ret
        
;Reads the mouse and updates the cursor X/Y and mouse bits accordingly.
;The interface must have been initialized before and its presence checked.
;Screen limits are not tested here.
AlbireoManageMouse:
        ;Don't wait here, just test the bit to see if data is ready.        
        ld bc,ACE_CMD
        in a,(c)
        rla
        ;Nothing to do? Exit.
        ret c
        
        ; If we get here, there is data waiting for us, so we need to read it
         ;Basic 340
        ld a,#27 : call AlbireoCmd
        
        ;Basic 370
        call AlbireoReadDat
        ;or a
        ;jp z,ScheduleTransfer ; In case we got no data, ask again (this is unlikely, I think this check can be removed)
        
        ;Reads Buttons.
        call AlbireoReadDat
        and %111                ;Only three buttons managed.
        ld (MouseButtonPressedBits),a
        
        ;Reads deltaX.
        call AlbireoReadDat
        or a
        jr z,.endXMotion
        call SignedByteToWord
        call MouseMoveHorizontally
.endXMotion
        ;Reads deltaY.
        call AlbireoReadDat
        or a
        jr z,.endYMotion
        call SignedByteToWord
        call MouseMoveVertically
.endYMotion
        ;Reads Wheel.
        call AlbireoReadDat
             
        ;Schedules a new data transfer for the next time (next VBL?).
ScheduleTransfer:    
        ;Basic 230
        ld a,#4e : call AlbireoCmd
        
        ;Basic 250
Token:  ld a,0
        call AlbireoDat
        ld a,(Token + 1)
        xor #80
        ld (Token + 1),a
        
        ;Basic 300
        ld a,#19
        jr AlbireoDat


;Sends a byte to the CMD port.
;IN:    A = the byte to send.
AlbireoCmd:
        ld bc,ACE_CMD
        out (c),a
        ret
        
;OUT:   A = the byte read from the DAT.
AlbireoReadDat:
        ld bc,ACE_DAT
        in a,(c)
        ret
        
;Waits for the command to end. There is no timeout.
AlbireoWaitForCommandEnd:
        ld bc,ACE_CMD
.waitLoop
        in a,(c)
        rla
        jr c,.waitLoop
        
        ld a,#22
        jp AlbireoCmd