
;Detection snippets by TFM and Prodatron.
;Mouse docs: http://www.cpcwiki.eu/index.php/SYMBiFACE_II:PS/2_mouse


;Detects and initializes the SF2.
;OUT:   Carry=1=detected.
InitializeSymbiface2:
        ;Code by Prodatron. This only detects the mouse!
        ld bc,#fd10
        ld a,100
.loop   in d,(c)
        dec a
        jr nz,.loop
        in a,(c)
        scf
        ret z
        ccf
        ret

;Other method by TFM. Probably more reliable, but WARNING, this code changes the millenium byte of the RTC of the SF2 to 20!     
        ;ld a,#32
        ;ld bc,#fd15
        ;out (c),a               ;Select Millenium
        ;dec c
        ;out (c),c
        ;inc c
        ;out (c),a
        ;dec c
        ;in a,(c)
        ;cp c
        ;jr nz,.notPresent
        ;scf
        ;ret
        
;.notPresent
 ;       or a
 ;       ret
        
;Reads the mouse and updates the cursor X/Y and mouse bits accordingly.
;The interface must have been initialized before and its presence checked.
;Screen limits are not tested here.
Symbiface2ManageMouse:
        ;H = accumulated X, L = accumulated Y.
        ld hl,0

        ;As long as "mode" is not 0, accumulate the data.
        ld bc,#fd10
.mouseLoop:
        in a,(c)
        ld d,a
        and %11000000
        jr nz,.moreData
  
        ;No more data.
        ;No X/Y offset were found? Then stops.
        ld a,l
        or h
        ret z

        ;Converts 8 bits to signed 16 bits to add the coordinates, and exits.
        ld a,h
        call SignedByteToWord
        ld b,l          ;Saves L.
        call MouseMoveHorizontally
        
        ld a,b
        call SignedByteToWord
        jp MouseMoveVertically
        
.moreData
        ld a,d
        and %11000000
        cp %01000000
        jr z,.xOffsetFound
        cp %10000000
        jr z,.yOffsetFound

        ;Buttons if bit 5 is 0, else it is wheel, which is not managed here.
        bit 5,d
        jr nz,.mouseLoop
        ;Buttons, not wheel.
        ld a,d
        and %00011111
        ld (MouseButtonPressedBits),a
        
        jr .mouseLoop

        
.xOffsetFound
        ld a,d
        and %00111111
        rla                     ;Bit 7 and 6 are given the same value as bit 5 (which is the sign).
        rla
        sra a
        sra a
        add a,h
        ld h,a
        jr .mouseLoop
         
.yOffsetFound
        ld a,d
        and %00111111
        rla                     ;Bit 7 and 6 are given the same value as bit 5 (which is the sign).
        rla
        sra a
        sra a
        neg
        add a,l
        ld l,a
        jr .mouseLoop
                