        ;Mouse test.
        ;By Targhan/Arkos, April 2020.

        ;Thanks to Pulkomandy, TFM, Prodatron and Toto for their code snippets and support.

        ;Compiles with Rasm.

        ;Supported hardware:
        ;- CPC cursor (yay!).
        ;- Joystick 1
        ;- Joystick 2
        ;- Albireo
        ;- Symbiface 2
        ;- Symbiface 3
        ;- Multiplay mouse.
        ;- Multiplay joystick.

        ;F1/F2 to decrease/increase the mouse speed X ratio.
        ;F4/F5 to decrease/increase the mouse speed Y ratio.

        ;Terminology:
        ;- What I call "mouse" is any device which handles a real speed.
        ;- What I call "cursor" is what does not, so it can be the joystick, the keyboard, and even AMX mouse or anything that plugs into the joystick port.

        ;See the flags at the bottom to understand better how it works.
        ;A possible UI should update some of them (as explained in their comment).

        ;To optimize and simplify click management, as soon as an interface is detected and enabled by the user, the other interfaces are skipped.
        ;At the end, two of their button flags are mapped into primary/secondary buttons (according to the user's choice).
        ;This should cover most needs, but feel free to adapt to your needs.

        ;LIMITATIONS:
        ;The mouse wheel is managed by each interface, but the result is neither stored not displayed (easy to develop).
        ;In this code, only one Multiplay joystick and one mouse at the same time are tested. Once again, feel free to modify to fit your needs.

        ;On the screen, you will see:
        ;- 5 raw bytes at the top-left whenever a mouse button is clicked (one dot per button).
        ;- Middle top, 2 dots for primary/secondary click.





ScreenWidthInBytes: equ 80
ScreenWidthInPixels: equ ScreenWidthInBytes * 2         ;Mode 0.
ScreenHeight: equ 200

        org #1000

        di
        ld hl,#c9fb
        ld (#38),hl
        ;Mode 0.
        ld bc,#7f8c
        out (c),c
        ld sp,$

        ld hl,#c000
        ld de,#c001
        ld bc,#3fff
        ld (hl),l

        ld bc,#7f10
        out (c),c
        ld a,#54
        out (c),a

        ;All the inks to white.
        ld hl,#0f4b
.loopColors out (c),h
        out (c),l
        dec h
        jr nz,.loopColors

        call GenerateScreenTable

        ;Tests the presence of the interfaces, and initializes them if found.
        call InitializeAlbireo
        rla
        ld (AlbireoDetected),a

        call InitializeSymbiface2
        rla
        ld (Symbiface2Detected),a

        call InitializeSymbiface3
        rla
        ld (Symbiface3Detected),a

        call InitializeMultiplay
        rla
        ld (MultiplayDetected),a


MainLoop:

VSync   ld b,#f5
        in a,(c)
        rra
        jr nc,VSync + 2

        ei
        nop
        halt
        halt
        di

        call Border26

        ;Clears the button states.
        xor a
        ld (PrimaryButton),a
        ld (SecondaryButton),a

        ;Manages each interface in turn. Skips the others as soon as one is both detected and authorized by the user.

        ;Albireo is detected and can be used?
        ld a,(AlbireoDetected)
        rra
        jr nc,.albireoEnd
        ld a,(UseAlbireo)
        or a
        jr z,.albireoEnd
        call AlbireoManageMouse
        jr .interfacesEnd             ;Skips the other interfaces if this one has been managed.
.albireoEnd

        ;Symbiface2 is detected and can be used?
        ld a,(Symbiface2Detected)
        rra
        jr nc,.symbiface2End
        ld a,(UseSymbiface2)
        or a
        jr z,.symbiface2End
        call Symbiface2ManageMouse
        jr .interfacesEnd             ;Skips the other interfaces when this one has been managed.
.symbiface2End

        ;Symbiface3 is detected and can be used?
        ld a,(Symbiface3Detected)
        rra
        jr nc,.symbiface3End
        ld a,(UseSymbiface3)
        or a
        jr z,.symbiface3End
        call Symbiface3ManageMouse
        jr .interfacesEnd             ;Skips the other interfaces when this one has been managed.
.symbiface3End

        ;Multiplay mouse must be used?
        ld a,(MultiplayDetected)
        rra
        jr nc,.multiplayEnd
        ld a,(UseMultiplayMouse)
        or a
        jr z,.multiplayMouseEnd
        ld a,(MultiplayMousePortIndex)
        call MultiplayManageMouse
.multiplayMouseEnd

        ;Multiplay joystick must be used?
        ld a,(UseMultiplayJoystick)
        or a
        jr z,.multiplayJoystickEnd
        ld a,(MultiplayJoystickPortIndex)
        call MultiplayManageJoystick
        ;jr .interfacesEnd             ;Skips the other interfaces when this one has been managed.
.multiplayJoystickEnd
.multiplayEnd

.interfacesEnd

        call Border20

        ;We test the keyboard anyway.
        call ManageKeyboardAndJoystick

        call Border0

        call CheckIfNewRatioFromKeyboard

        ;Checks whether cursor is still within the screen limits.
        call CorrectCursorCoordinates

        ;Removes the cursor at his old position, then displays it in its new.
        ld hl,(CursorScreenAddress)
        ld (hl),0
        call DisplayCursor

        ;Maps the mouse buttons to primary/secondary buttons, if present.
        ld a,(MouseButtonPressedBits)
        ld b,a
        ld a,(MouseButtonBitIndexToPrimaryButton)
        and b                                           ;If the mouse bit is pressed, and that it matches the wanted primary button, then A is not 0.
        jr z,.afterMapMousePrimaryButton
        ld (PrimaryButton),a
.afterMapMousePrimaryButton
        ld a,(MouseButtonBitIndexToSecondaryButton)     ;The same for the secondary button.
        and b
        jr z,.afterMapMouseSecondaryButton
        ld (SecondaryButton),a
.afterMapMouseSecondaryButton


        ;Inverts the primary/secondary buttons if wanted.
        ld a,(InvertPrimaryAndSecondaryButtons)
        or a
        jr z,.afterInvertButtons
        ld hl,(PrimaryButton)     ;This works because the values are one after the other.
        ld a,l
        ld l,h
        ld h,a
        ld (PrimaryButton),hl
.afterInvertButtons

        ;Any click from the mouse? Shows the raw button result on the screen (only for debugging!). Spread each bit to a byte.
        ld ix,#c000
        ld a,(MouseButtonPressedBits)
        ld c,a
        and %00001
        ld (ix + 0),a
        ld a,c
        and %00010
        ld (ix + 1),a
        ld a,c
        and %00100
        ld (ix + 2),a
        ld a,c
        and %01000
        ld (ix + 3),a
        ld a,c
        and %10000
        ld (ix + 4),a           ;5 Buttons shown.


        ld a,(PrimaryButton)
        ld (#c028),a
        ld a,(SecondaryButton)
        ld (#c029),a

        jp VSync

Border26:
        ld bc,#7f10
        out (c),c
        ld a,#4b
        out (c),a
        ret
Border20:
        ld bc,#7f10
        out (c),c
        ld a,#53
        out (c),a
        ret
Border0:
        ld bc,#7f10
        out (c),c
        ld a,#54
        out (c),a
        ret

;Checks the keyboard keys and updates the cursor X/Y accordingly.
;Screen limits are not tested here.
ManageKeyboardAndJoystick:
        call FillKeyboardLines

        ;Gets the bits from the possible other Joystick interfaces (Multiplay for example).
        ;They will be mixed with the native cursor/joystick events.
        ld hl,JoystickEventsFromInterface
        ld e,(hl)       ;E = b0 = up? b1 = down? b2 = left? b3 = right? b4 = fire1? b5 = fire2?
        ld (hl),0       ;Clears it! We don't want them to stack up on next frame.


        ;Gathers the directions.

        ;Up cursor?
        ld a,(KeyboardLine0)
        rra
        jr c,.noUpCursor
        set 0,e
.noUpCursor
        ;Right?
        rra
        jr c,.noRightCursor
        set 3,e
.noRightCursor
        ;Down?
        rra
        jr c,.noDownCursor
        set 1,e
.noDownCursor


        ld a,(KeyboardLine1)
        ;Left cursor?
        rra
        jr c,.noLeftCursor
        set 2,e
.noLeftCursor
        ;Copy? (=fire 2)
        rra
        jr c,.noCopyCursor
        set 5,e
.noCopyCursor

        ld a,(KeyboardLine5)
        ;Space? (=fire 1)
        rla
        jr c,.noSpaceCursor
        set 4,e
.noSpaceCursor


        ;Joystick 1.
        ld a,(KeyboardLine9)
        ;Up J1?
        rra
        jr c,.noUpJ1
        set 0,e
.noUpJ1
        ;Down J1?
        rra
        jr c,.noDownJ1
        set 1,e
.noDownJ1
        ;Left J1?
        rra
        jr c,.noLeftJ1
        set 2,e
.noLeftJ1
        ;Right J1?
        rra
        jr c,.noRightJ1
        set 3,e
.noRightJ1
        ;Fire1 J1?
        rra
        jr c,.noFire1J1
        set 4,e
.noFire1J1
        ;Fire2 J1?
        rra
        jr c,.noFire2J1
        set 5,e
.noFire2J1




        ;Joystick 2, if enabled.
        ld a,(EnableJoystick2)
        or a
        jr z,.endJoystick2

        ld a,(KeyboardLine6)
        ;Up J2?
        rra
        jr c,.noUpJ2
        set 0,e
.noUpJ2
        ;Down J2?
        rra
        jr c,.noDownJ2
        set 1,e
.noDownJ2
        ;Left J2?
        rra
        jr c,.noLeftJ2
        set 2,e
.noLeftJ2
        ;Right J2?
        rra
        jr c,.noRightJ2
        set 3,e
.noRightJ2
        ;Fire1 J2?
        rra
        jr c,.noFire1J2
        set 4,e
.noFire1J2
        ;No fire 2 with J2.
.endJoystick2

        ;Aggregates the result.

        ld c,e
        ;C = b0 = up? b1 = down? b2 = left? b3 = right? b4 = fire1? b5 = fire2?
        ;If no horizontal/vertical, the speed must be set to 0.
        ld a,c
        and %001111
        jr nz,.startDirection
        ld hl,0
        ld (CursorSpeed),hl
        jr AfterCursorVertical

.startDirection
        ;Here we know a direction is done.
        call IncreaseAcceleration

        ;Horizontal.
        bit 2,c
        jr nz,CursorMoveLeft
        bit 3,c
        jr nz,CursorMoveRight
AfterCursorHorizontal

        ;Vertical.
        bit 0,c
        jr nz,CursorMoveUp
        bit 1,c
        jr nz,CursorMoveDown
AfterCursorVertical

        ld a,255        ;Used for fire.
        ;Fire 1?
        bit 4,c
        jr z,.afterFire1
        ld (PrimaryButton),a
.afterFire1
        ;Fire 2?
        bit 5,c
        jr z,.afterFire2
        ld (SecondaryButton),a
.afterFire2

        ret

;The following methods must NOT modify C!
CursorMoveRight:
        ld a,(CursorSpeed + 1)     ;Only keeps the integer part.
CursorMoveHorizontallyPerform:
        call SignedByteToWord
        call CursorMoveHorizontally

        jr AfterCursorHorizontal

CursorMoveLeft:
        ld a,(CursorSpeed + 1)     ;Only keeps the integer part.
        neg
        jr CursorMoveHorizontallyPerform



CursorMoveUp:
        ld a,(CursorSpeed + 1)     ;Only keeps the integer part.
        neg
CursorMoveVerticallyPerform:
        add a,a
        call SignedByteToWord
        call CursorMoveVertically

        jr AfterCursorVertical

CursorMoveDown:
        ld a,(CursorSpeed + 1)     ;Only keeps the integer part.
        jr CursorMoveVerticallyPerform

;Increases the Acceleration, with a limit.
IncreaseAcceleration:
        ld a,(CursorSpeedMaximumInteger)
        ld b,a

        ld hl,(CursorSpeed)
        ld de,(CursorAccelerationRate)
        add hl,de
        ld a,h
        cp b
        jr c,.notMax
        ld h,b
.notMax
        ld (CursorSpeed),hl
        ret


;Updates the X of the cursor position.
;This should be called only by the keyboard/joystick methods, as mouse must be applied a specific user ratio.
;IN:    DE = The amount of X to add. May be negative.
;MOD:   HL.
CursorMoveHorizontally:
        ld hl,(CursorX)
        add hl,de
        ld (CursorX),hl
        ret

;Updates the Y of the cursor position.
;This should be called only by the keyboard/joystick methods, as mouse must be applied a specific user ratio.
;IN:    DE = The amount of Y to add. May be negative.
;MOD:   HL.
CursorMoveVertically:
        ld hl,(CursorY)
        add hl,de
        ld (CursorY),hl
        ret

;Updates the X of the mouse position. Takes care of the ratio defined by the user.
;IN:    DE = The amount of X to add. May be negative.
;MOD:   HL, A.
MouseMoveHorizontally:
        ld a,(MouseXRatio)
        call ApplyMouseRatio

        ld hl,(CursorX)
        add hl,de
        ld (CursorX),hl
        ret
;Updates the Y of the mouse position. Takes care of the ratio defined by the user.
;IN:    DE = The amount of Y to add. May be negative.
;MOD:   HL, A.
MouseMoveVertically:
        ld a,(MouseYRatio)
        call ApplyMouseRatio

        ld hl,(CursorY)
        add hl,de
        ld (CursorY),hl
        ret

;IN:    DE = a positive or negative number.
;       A = The ratio value (2 = normal).
ApplyMouseRatio:
        cp 2    ;Normal, nothing to do.
        ret z

        or a
        jr nz,.noDiv4
        ;/4
        sra d
        rr e
        sra d
        rr e
        ret
.noDiv4
        dec a
        jr nz,.noDiv2
        ;/2
        sra d
        rr e
        ret
.noDiv2
        sub 2
        jr nz,.noMul2
        ;*2
        sla e
        rl d
        ret
.noMul2
        ;*4.
        sla e
        rl d
        sla e
        rl d
        ret

Error:
        ld bc,#7f10
        out (c),c
        ld a,#4c
        out (c),a
        jr $




;Generates a table for each line of the screen, for a base screen of #c000.
GenerateScreenTable:
        ld ix,ScreenLineAddresses
        ld de,#c000 + ScreenWidthInBytes
        ld b,ScreenHeight

        ld hl,#c000
.loop
        ld (ix + 0),l
        ld (ix + 1),h
        inc ix
        inc ix

        ;Next line.
        ld a,h
        add a,8
        ld h,a
        jr nc,.noOverflow
        add hl,de
.noOverflow
        djnz .loop
        ret


;Reads the useful lines of the keyboards.
FillKeyboardLines:
	ld bc,#f40e
	out (c),c
	ld bc,#f6c0
	out (c),c
	out (c),0
	ld bc,#f792
	out (c),c

        ;Reads the interesting lines.
        ld de,#f4f6

        MACRO ReadKeyboardLine LineNumber
                ;L must be line number + 64.
                ld b,e          ;#f6
                out (c),l
                ld b,d          ;#f4
                in a,(c)
                ld (KeyboardLine{LineNumber}),a
        ENDM
        ld l,0 + 64
        ReadKeyboardLine 0
        inc l
        ReadKeyboardLine 1
        inc l
        ReadKeyboardLine 2
        ld l,5 + 64
        ReadKeyboardLine 5
        inc l
        ReadKeyboardLine 6
        ld l,9 + 64
        ReadKeyboardLine 9

	ld bc,#f782
	out (c),c
	dec b
	out (c),0
	ret

;Displays the cursor from its position. Also updates the cursor address for removal.
DisplayCursor:
        ld hl,(cursorY)
        add hl,hl
        ld de,ScreenLineAddresses
        add hl,de
        ld e,(hl)
        inc hl
        ld d,(hl)               ;DE = Address of the start of line.

        ;What X?
        ld hl,(CursorX)
        ld a,l
        srl h
        rr l
        add hl,de               ;HL = screen location.

        ;What byte to display?
        ld bc,%10101010 * 256 + %01010101
        rra
        jr c,.leftPixel
        ld c,b
.leftPixel
        ld (hl),c

        ;Updates the last cursor address, for removal.
        ld (CursorScreenAddress),hl
        ret

;Corrects the cursor position on the screen bounds.
CorrectCursorCoordinates:
        ;X too "on the left"?
        ld hl,(CursorX)
        bit 7,h
        jr z,.cursorXPositive
        ld hl,0
        ld (CursorX),hl
        jr .endCursorX
.cursorXPositive
        ;X too "on the right"?
        ld de,ScreenWidthInPixels
        or a
        sbc hl,de
        jr c,.endCursorX
        ld hl,ScreenWidthInPixels - 1
        ld (CursorX),hl
.endCursorX

        ;Y too "high"?
        ld hl,(CursorY)
        bit 7,h
        jr z,.cursorYPositive
        ld hl,0
        ld (CursorY),hl
        ret
.cursorYPositive
        ;Y too "at the bottom"?
        ld de,ScreenHeight
        or a
        sbc hl,de
        ret c
        ld hl,ScreenHeight - 1
        ld (CursorY),hl
        ret


;Converts a signed byte to a signed word.
;IN:    A  = a signed number.
;OUT:   DE = The signed number, stretched to 16 bits.
SignedByteToWord:
        ld e,a
        ld d,0
        rla
        ret nc
        dec d
        ret

;Checks if the user has changed the ratio.
CheckIfNewRatioFromKeyboard:
.keyBusy: ld a,0
        or a
        jr z,.canContinue
        ;The keys must be unpressed, else the ratio will change too quickly.
        ld a,(KeyboardLine0)
        cp 255
        ret nz
        ld a,(KeyboardLine1)
        cp 255
        ret nz
        ld a,(KeyboardLine2)
        cp 255
        ret nz
.canContinue

        ;Any ratio from the user?
        ld hl,MouseXRatio
        ld a,(KeyboardLine1)
        bit 5,a                         ;F1: decrease X ratio.
        jr z,.decreaseRatio
        bit 6,a                         ;F2: increase X ratio.
        jr z,.increaseRatio

        ld hl,MouseYRatio
        ld a,(KeyboardLine2)
        bit 4,a                         ;F4: decrease Y ratio.
        jr z,.decreaseRatio
        ld a,(KeyboardLine1)
        bit 4,a                         ;F5: increase Y ratio.
        jr z,.increaseRatio

        ;None of these keys are pressed.
        xor a
        ld (.keyBusy + 1),a
        ret

        ;HL = points on the ratio to modify.
.increaseRatio
        ld a,(hl)
        cp 4                    ;Upper limit check.
        ret z
        inc a
        jr .setRatio
.decreaseRatio
        ;HL = points on the ratio to modify.
        ld a,(hl)
        sub 1
        jr c,.endRatio          ;Lower limit check.
.setRatio ld (hl),a

        ;Prevents too quick key press.
.endRatio
        ld a,1
        ld (.keyBusy + 1),a
        ret

        include "Albireo.asm"
        include "Symbiface2.asm"
        include "Symbiface3.asm"
        include "Multiplay.asm"



;The following data can (should?) be parameterized by your user interface.
;--------------------------------------------------------------
EnableJoystick2: db 1                                   ;!=0 if Joystick 2 can be used.
CursorAccelerationRate: dw #20                          ;The higher, the faster.
CursorSpeedMaximumInteger: db 3                         ;The maximum speed of the cursor (integer part).

;Mouse (only) ratio: 0 = /4, 1 = /2, 2 = normal, 3 = *2, 4 = *4
MouseXRatio: db 2
MouseYRatio: db 2
InvertPrimaryAndSecondaryButtons:     db 0              ;!=0 to invert the primary/secondary cursor buttons (useful for AMX).
MouseButtonBitIndexToPrimaryButton:   db %00001         ;What button of the mouse must considered the primary button? Bit 0 = button 0, etc.
MouseButtonBitIndexToSecondaryButton: db %00010         ;What button of the mouse must considered the secondary button? Bit 0 = button 0, etc.

;The first interface detected and to be used will dismiss the others.
;To avoid confusion, your UI should probably allow to select only one interface among the ones detected.
UseAlbireo: db 1                                ;!=0 to use the Albireo (if detected).
UseSymbiface2: db 1                             ;!=0 to use the Symbiface2 (if detected).
UseSymbiface3: db 1                             ;!=0 to use the Symbiface3 (if detected).
UseMultiplayMouse: db 1                         ;!=0 to use the Multiplay Mouse. Must be exclusive from the Multiplay Joystick on the SAME port, else conflicts!
UseMultiplayJoystick: db 0                      ;!=0 to use the Multiplay Joytick. Must be exclusive from the Multiplay Mouse on the SAME port, else conflicts!

MultiplayMousePortIndex: db 0                   ;0 for port A, 1 for port B. NOT another value!
MultiplayJoystickPortIndex: db 0                ;0 for port A, 1 for port B. NOT another value!

;--------------------------------------------------------------


AlbireoDetected: db 0                           ;Bit 0 = is Albireo detected? Other bits are random.
Symbiface2Detected: db 0                        ;Bit 0 = is SF2 detected? Other bits are random.
Symbiface3Detected: db 0                        ;Bit 0 = is SF3 detected? Other bits are random.
MultiplayDetected: db 0                         ;Bit 0 = is Multiplay detected? Other bits are random.

CursorSpeed: dw 0                               ;The speed of the cursor (integer and decimal part).


;PPI keyboard lines.
KeyboardLine0: db 255
KeyboardLine1: db 255
KeyboardLine2: db 255
KeyboardLine5: db 255
KeyboardLine6: db 255
KeyboardLine9: db 255

JoystickEventsFromInterface: db 0               ;Joysticks events from various interface (Multiplay for example). b0 = up? b1 = down? b2 = left? b3 = right? b4 = fire1? b5 = fire2?

CursorX: dw ScreenWidthInPixels / 2             ;In pixels.
CursorY: dw ScreenHeight / 2
;Mouse buttons are physical. Any of them will be mapped later into the primary or secondary, according to the user's choice.
;Technical note: this must not be cleared every frame, because interfaces only notifies changes.
MouseButtonPressedBits: db 0                    ;Bit 0 if mouse button 0 is clicked, bit 1 if mouse button 1 is clicked, etc. Up to bit 4 (so 5 buttons managed).

;These two values must be in a row.
PrimaryButton: db 0                             ;!=0 if the primary button is clicked. Keyboard/joystick fire sets this value directly.
SecondaryButton: db 0                           ;!=0 if the secondary button is clicked. Keyboard/joystick fire sets this value directly.
        ASSERT (SecondaryButton - PrimaryButton) == 1

CursorScreenAddress: dw #c000                   ;Useful to delete it easily.

;The start of each line. Warning, base address = #0000.
ScreenLineAddresses: ds ScreenHeight * 2, 0
