        ;Manages Toto's Multiplay (two ports, each for either a mouse or a joystick).
        ;Note that its presence cannot be detected.

MultiplayActionsBitsPortA       equ #f990  ;+1 for port B.
MultiplayMouseXPortA            equ #f992  ;+2 for port B.
MultiplayMouseYPortA            equ #f993  ;+2 for port B.


;Detects and initializes the Multiplay.
;There is no real way to detect it, but on a CPC, reading the actions bits often returns #ff when not present
;and bit 7 is always 0 according to the doc. Plus, on a joystick, it is not possible to go up and down or left
;and right at the time. This is what is tested.
;OUT:   Carry=1=detected.
InitializeMultiplay:
        ;Tests anomalies in port A.
        ld bc,MultiplayActionsBitsPortA
        call .testPresence
        ret nc          ;Not present for port A? Then exits directly.

        ;Tests anomalies in port B.
        inc c
        ;Carry=1=detected.
        ;BC=action port (A or B).
.testPresence
        in a,(c)
        rla
        jr c,.notPresent        ;Bit 7 must be 0. If not, the card is not here.
        ;After shift: bit1: Up, bit2: Down, bit3: Left, bit4: Right.
        ;It is not possible to have bit up/down and left/right.
        ld l,a
        and %00110
        cp %00110
        ret z
        ld a,l
        and %11000
        cp %11000
        ret z

        scf
        ret
.notPresent
        or a
        ret



;Reads the mouse and updates the cursor X/Y and mouse bits accordingly.
;The interface must have been initialized before and its presence checked.
;Screen limits are not tested here.
;IN:    A = which port (0 = A, 1 = B)? Do NOT use any other value.
MultiplayManageMouse:
        ld l,a
        ;Reaches the right port for the mouse buttons.
        ld bc,MultiplayActionsBitsPortA
        add c
        ld c,a

        in a,(c)
        rra
        rra
        rra
        rra
        and %111
        ld (MouseButtonPressedBits),a

        ;Goes to the Mouse X port.
        ASSERT (MultiplayActionsBitsPortA + 2) == MultiplayMouseXPortA
        inc c
        inc c
        ld a,c          ;If Port A, L is 0, so add nothing.
        add a,l         ;If Port B, L is 1, add 1, to reach #f994, the Mouse X for Port B.
        ld c,a

        in a,(c)
        call SignedByteToWord
        call MouseMoveHorizontally

        ;Goes to the Mouse Y port.
        ASSERT (MultiplayMouseXPortA + 1) == MultiplayMouseYPortA
        inc c
        in a,(c)
        call SignedByteToWord
        jp MouseMoveVertically


;Reads the joystick and updates the cursor X/Y. Fire bits are directly put in the mouse bits.
;IN:    A = which port (0 = A, 1 = B)? Do NOT use any other value.
;Will set JoystickEventsFromInterface and the MouseButtonPressedBits.
MultiplayManageJoystick:
        ;Reaches the right port for the Joystick.
        ld bc,MultiplayActionsBitsPortA
        add c
        ld c,a

        ;b0 = up? b1 = down? b2 = left? b3 = right? b4 = fire1? b5 = fire2? b6 = fire3?
        in a,(c)
        ld c,a
        and %1111       ;Stores only the directions. The click bits will be considered as Mouse click, because there are 3 buttons.
        ld (JoystickEventsFromInterface),a

        ;Remaining bits are the mouse bits.
        ld a,c
        rra
        rra
        rra
        rra
        and %111
        ld (MouseButtonPressedBits),a

        ret
