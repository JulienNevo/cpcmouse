
;Based on the original snippets by TFM.


;Detects and initializes the SF3.
;OUT:   Carry=1=detected.
InitializeSymbiface3:
        ld bc,#fd4f
        out (c),c               ;SF3 echo test port.
        
        in a,(c)
        cp c
        jr nz,.notPresent
        scf
        ret
.notPresent
        or a
        ret
        
        
;Reads the mouse and updates the cursor X/Y and mouse bits accordingly.
;The interface must have been initialized before and its presence checked.
;Screen limits are not tested here.
Symbiface3ManageMouse:
        call Symbiface3WaitTillReady
        
        ld a,20
        out (c),a
        inc c           ;BC = #fd42
        
        in a,(c)        ;A = X offset.
        jr z,.afterXOffset
        call SignedByteToWord           ;Converts 8 bits to signed 16 bits to add the coordinates, and exits.
        call MouseMoveHorizontally
.afterXOffset
        
        in a,(c)        ;A = Y offset.
        jr z,.afterYOffset
        call SignedByteToWord           ;Idem.
        call MouseMoveVertically
.afterYOffset

        in a,(c)          ;A = buttons 0-4. Bit 0 = Button 0.
        and %11111
        ld (MouseButtonPressedBits),a
        
        in a,(c)        ;Wheel.
        ret
        
        
;Waits till SF3 is ready.
;OUT:   BC = #FD41
;       A  = SF3 status register.
Symbiface3WaitTillReady:
        ld bc,#fd41     ;SF3 Command & Status Register.
.loop   in a,(c)
        dec a
        jr z,.loop
        
        inc a
        ret